import { FreeSwitchPluginConfig } from '../../lib';

export default (): FreeSwitchPluginConfig => {
  return {
    externalHost: 'http://localhost',
    recordingsPath: '/var/lib/freeswitch/recordings/{DOMAIN}/archive/{YEAR}/{MONTH}/{DAY}/{UUID}.wav',
  };
};
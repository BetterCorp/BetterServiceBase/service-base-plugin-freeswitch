import { CPlugin } from '@bettercorp/service-base/lib/ILib';
import { Tools } from '@bettercorp/tools/lib/Tools';
import * as ESL from 'modesl';
import { FreeSwitchESLExecute, FreeSwitchESLExecuteType, FreeSwitchESLPluginConfig, FreeSwitchESLServerEvent, FreeSwitchESLServerEventCustomFunctions, FreeSwitchESLServerEventNames, FreeSwitchESLServerEventTrigger, FreeSwitchESLSMS, FreeSwitchPluginESLEvents } from '../../lib';
import { FreeSwitchEvents } from './events';

export class Plugin extends CPlugin<FreeSwitchESLPluginConfig> {
  /*initForPlugins<T1 = FreeSwitchESLInitPlugin, T2 = void> (initType: string, args: T1): Promise<T2> {
    const self = this;
    return new Promise((resolve) => {
      let argsAs = args as unknown as IWebServerInitPlugin;
      self.Features.log.debug(`initForPlugins [${initType}] ${argsAs.arg1}`);
      if (Tools.isNullOrUndefined(argsAs.arg2))
        (this.Express as any)[initType](argsAs.arg1);
      else
        (this.Express as any)[initType](argsAs.arg1, argsAs.arg2);
      self.Features.log.debug(`initForPlugins [${initType}] - OKAY`);
      (resolve as any)();
    });
  }*/
  private objectToString(args: any): string {
    const resStr: Array<string> = [];
    Object.keys(args).map((k, index) => {
      resStr.push(args[k]);
    });
    return resStr.join(' ');
  }
  private getHeader(headers: any, headerName: string): string {
    let result = '';
    for (let i = 0; i < headers.length; i++) {
      const item = headers[i];
      if (item.name === headerName) {
        result = item.value;
        break;
      }
    }
    return `${ result || '' }`;
  }
  init(): Promise<void> {
    const self = this;
    return new Promise(async (resolve) => {
      // SERVER
      if (self.getPluginConfig().server.enabled) {
        const ESLServer = new ESL.Server({
          host: self.getPluginConfig().server.host,
          port: self.getPluginConfig().server.port,
          myevents: self.getPluginConfig().server.myEvents || true
        }, function () {
          self.log.info('FreeSwitch ESL Server Started');
        });
        ESLServer.on('connection::ready', async (conn: ESL.Connection, id: string) => {
          self.log.info('New call: ' + id);
          try {
            let currentEvent: FreeSwitchESLServerEvent | null = null;
            do {
              self.log.info('Get identifation action: ' + id);
              self.log.debug(' -- LAST EVENT');
              self.log.debug(currentEvent || 'null');
              self.log.debug(' -- LAST EVENT');
              currentEvent = await self.emitEventAndReturn<FreeSwitchESLServerEventTrigger, FreeSwitchESLServerEvent>(null, FreeSwitchPluginESLEvents.OnESLServerEvent, {
                lastEvent: currentEvent || null,
                id,
                callInfo: conn.getInfo()
              });
              self.log.info('Got identifation action[' + currentEvent.name + ']: ' + id);
              self.log.debug(' -- NEW EVENT');
              self.log.debug(currentEvent);
              self.log.debug(' -- NEW EVENT');
              switch (currentEvent.name) {
                case FreeSwitchESLServerEventNames.continue:
                  self.log.info('Got identifation action[' + currentEvent.name + ']: ' + id + ' = OKAY');
                  currentEvent = null;
                  break;
                case FreeSwitchESLServerEventNames.hangUp:
                  currentEvent!.response = await (new Promise((r: any) => conn.execute('hangup', currentEvent!.hangup?.reason, id, r)));
                  self.log.info('Got identifation action[' + currentEvent.name + ']: ' + id + ' = OKAY');
                  currentEvent = null;
                  break;
                case FreeSwitchESLServerEventNames.answer:
                  currentEvent!.response = await (new Promise((r: any) => conn.execute('answer', undefined, id, r)));
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = OKAY');
                  break;
                case FreeSwitchESLServerEventNames.playback:
                  if (!Tools.isNullOrUndefined(currentEvent.playback?.terminators)) {
                    await (new Promise((r: any) => conn.execute('set', `playback_terminators=${ currentEvent!.playback?.terminators }`, id, r)));
                    self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = TERMINATORS SET');
                  }
                  // TODO: GET AUDIO IF REMOTE
                  currentEvent!.response = await (new Promise((r: any) => conn.execute('playback', currentEvent!.playback?.file, id, r)));
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = OKAY');
                  break;
                case FreeSwitchESLServerEventNames.ivr:
                  const evt: any = await (new Promise((r: any) => conn.execute('play_and_get_digits', self.objectToString({
                    min: currentEvent!.ivr?.minDigits,
                    max: currentEvent!.ivr?.maxDigits,
                    tries: currentEvent!.ivr?.retries,
                    timeout: currentEvent!.ivr?.timeout,
                    terminators: currentEvent!.ivr?.terminator,
                    file: currentEvent!.ivr?.playFile,
                    invalid_file: currentEvent!.ivr?.invalidFile,
                    var_name: 'ivr_input_digit',
                    regexp: '\\d+',
                    digit_timeout: currentEvent!.ivr?.digitTimeout,
                  }), id, r)));
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = INFO');
                  currentEvent!.response = evt;
                  const input = self.getHeader(evt.headers, 'variable_ivr_input_digit');
                  currentEvent.ivr!.responseFailure = true;
                  if (input !== '') {
                    currentEvent.ivr!.responseFailure = false;
                    currentEvent.ivr!.responseDigits = input;
                  }
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = OKAY');
                  break;
                case FreeSwitchESLServerEventNames.transfer:
                  currentEvent!.response = await (new Promise((r: any) => conn.api('uuid_transfer', [id, currentEvent!.transfer!.type, currentEvent!.transfer!.destination, 'xml', currentEvent!.transfer!.dialplan, currentEvent!.transfer!.domainName], r)));
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = OKAY');
                  break;
                case FreeSwitchESLServerEventNames.custom:
                  switch (currentEvent.custom!.function) {
                    case FreeSwitchESLServerEventCustomFunctions.api:
                      currentEvent!.response = await (new Promise((r: any) => conn.api(currentEvent?.custom?.command!, currentEvent?.custom?.args!, r)));
                      break;
                    case FreeSwitchESLServerEventCustomFunctions.bgapi:
                      currentEvent!.response = await (new Promise((r: any) => conn.bgapi(currentEvent?.custom?.command!, currentEvent?.custom?.args!, r)));
                      break;
                    case FreeSwitchESLServerEventCustomFunctions.execute:
                      currentEvent!.response = await (new Promise((r: any) => conn.execute(currentEvent?.custom?.app!, self.objectToString(currentEvent?.custom?.args!), id, r)));
                      break;
                    case FreeSwitchESLServerEventCustomFunctions.getInfo:
                      currentEvent!.response = conn.getInfo();
                      break;
                    case FreeSwitchESLServerEventCustomFunctions.originate:
                      currentEvent!.response = await (new Promise((r: any) => conn.originate(currentEvent!.custom!.options, r)));
                      break;
                  }
                  self.log.info('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = OKAY');
                  //await (conn as any)[item.function](item.args1, item.args2);
                  break;
                default:
                  currentEvent = null;
                  self.log.error('Got identifation action[' + currentEvent!.name + ']: ' + id + ' = BREAK NOT FOUND');
              }
            } while (currentEvent !== null);
            /*if (Tools.isArray(response)) {
              for (let item of response) {
                (conn as any)[item.function](item.args1, item.args2);
              }
            }*/
            //conn.execute('set', "note_note_note=test_test_test");
            //console.log(conn.getInfo());
            self.log.info('disconnect call ' + id);
            conn.disconnect();
          } catch (exc) {
            self.log.warn('disconnect call ' + id);
            self.log.warn(exc);
            conn.disconnect();
          }
          /*setTimeout(() => {
            features.log.info('disconnect call ' + id);
            conn.disconnect();
          }, 5000);*/
          /*conn.call_start = new Date().getTime();

          conn.execute('answer');
          conn.execute('echo', function () {
            features.log.info('echoing');
          });

          conn.on('esl::end', function (evt, body) {
            this.call_end = new Date().getTime();
            var delta = (this.call_end - this.call_start) / 1000;
            features.log.info("Call duration " + delta + " seconds");
          });*/
        });
      }

      // CLIENT
      if (self.getPluginConfig().client.enabled) {
        const fusion = new FreeSwitchEvents();
        await fusion.init(self,
          self.getPluginConfig().client.host,
          self.getPluginConfig().client.port,
          self.getPluginConfig().client.password,
          self.getPluginConfig().client.subscribe,
          self.getPluginConfig().client.customHeaders);

        if (self.getPluginConfig().client.subscribe === true || typeof self.getPluginConfig().client.subscribe === 'object') {
          fusion.emitEvent = (event: any) => {
            self.emitEvent(null, FreeSwitchPluginESLEvents.OnESLEvent, event);
          };
        }

        self.onReturnableEvent<FreeSwitchESLExecute>(null, FreeSwitchPluginESLEvents.Execute, (resolve: any, reject: any, data): any => {
          if (Tools.isNullOrUndefined(data)) return;
          if (Tools.isNullOrUndefined(data.type)) return;
          if (Tools.isNullOrUndefined(data.appOrCommand)) return;
          if (Tools.isNullOrUndefined(data.args)) return;
          if (data.type === FreeSwitchESLExecuteType.execute)
            return fusion.execute(data.appOrCommand, data.args, data.callId).then(resolve).catch(reject);
          if (data.type === FreeSwitchESLExecuteType.api)
            return fusion.api(data.appOrCommand, data.args).then(resolve).catch(reject);
          reject(`Unknown command [${ data.appOrCommand }]`);
        });

        self.onEvent(null, FreeSwitchPluginESLEvents.SendSMS, (data: FreeSwitchESLSMS) => {
          if (Tools.isNullOrUndefined(data)) return;
          if (Tools.isNullOrUndefined(data.to)) return;
          if (Tools.isNullOrUndefined(data.toProvider)) return;
          if (Tools.isNullOrUndefined(data.from)) return;
          if (Tools.isNullOrUndefined(data.profile)) return;
          if (Tools.isNullOrUndefined(data.body)) return;
          fusion.sendSMS(data.to, data.toProvider, data.from, data.profile, data.body);
        });
      }

      resolve();
    });
  }
}
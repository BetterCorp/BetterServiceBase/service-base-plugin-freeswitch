export interface ChannelData {
    state: string;
    direction: string;
    state_number: string;
    flags: string;
    caps: string;
}

export interface ErrorLog {
    start: any;
    stop: any;
    flaws: number;
    consecutiveFlaws: number;
    durationMS: number;
}

export interface Inbound {
    raw_bytes: number;
    media_bytes: number;
    packet_count: number;
    media_packet_count: number;
    skip_packet_count: number;
    jitter_packet_count: number;
    dtmf_packet_count: number;
    cng_packet_count: number;
    flush_packet_count: number;
    largest_jb_size: number;
    jitter_min_variance: number;
    jitter_max_variance: number;
    jitter_loss_rate: number;
    jitter_burst_rate: number;
    mean_interval: number;
    flaw_total: number;
    quality_percentage: number;
    mos: number;
    errorLog: Array<ErrorLog>;
}

export interface Outbound {
    raw_bytes: number;
    media_bytes: number;
    packet_count: number;
    media_packet_count: number;
    skip_packet_count: number;
    dtmf_packet_count: number;
    cng_packet_count: number;
    rtcp_packet_count: number;
    rtcp_octet_count: number;
}

export interface Audio {
    inbound: Inbound;
    outbound: Outbound;
}

export interface CallStats {
    audio: Audio;
}

export interface Variables {
    direction: string;
    is_outbound: string;
    uuid: string;
    session_id: string;
    sip_gateway_name: string;
    sip_invite_domain: string;
    sip_profile_name: string;
    text_media_flow: string;
    channel_name: string;
    sip_destination_url: string;
    'sip_h_P-Early-Media': string;
    max_forwards: string;
    originator_codec: string;
    originator: string;
    switch_m_sdp: string;
    export_vars: string;
    domain_name: string;
    origination_callee_id_name: string;
    call_direction: string;
    instant_ringback: string;
    originate_early_media: string;
    originating_leg_uuid: string;
    video_media_flow: string;
    rtp_local_sdp_str: string;
    sip_outgoing_contact_uri: string;
    sip_req_uri: string;
    sofia_profile_name: string;
    recovery_profile_name: string;
    sofia_profile_url: string;
    rtp_use_codec_string: string;
    remote_video_media_flow: string;
    remote_text_media_flow: string;
    remote_audio_media_flow: string;
    audio_media_flow: string;
    rtp_audio_recv_pt: string;
    rtp_use_codec_name: string;
    rtp_use_codec_rate: string;
    rtp_use_codec_ptime: string;
    rtp_use_codec_channels: string;
    rtp_last_audio_codec_string: string;
    read_codec: string;
    original_read_codec: string;
    read_rate: string;
    original_read_rate: string;
    write_codec: string;
    write_rate: string;
    dtmf_type: string;
    local_media_ip: string;
    local_media_port: string;
    advertised_media_ip: string;
    rtp_use_timer_name: string;
    rtp_use_pt: string;
    rtp_use_ssrc: string;
    remote_media_ip: string;
    remote_media_port: string;
    last_bridge_to: string;
    bridge_channel: string;
    bridge_uuid: string;
    signal_bond: string;
    sip_local_network_addr: string;
    sip_reply_host: string;
    sip_reply_port: string;
    sip_network_ip: string;
    sip_network_port: string;
    ep_codec_string: string;
    sip_user_agent: string;
    sip_allow: string;
    sip_recover_contact: string;
    sip_invite_route_uri: string;
    sip_invite_record_route: string;
    sip_full_via: string;
    sip_recover_via: string;
    sip_from_display: string;
    sip_full_from: string;
    sip_full_to: string;
    sip_from_user: string;
    sip_from_uri: string;
    sip_from_host: string;
    sip_to_user: string;
    sip_to_uri: string;
    sip_to_host: string;
    sip_contact_user: string;
    sip_contact_port: string;
    sip_contact_uri: string;
    sip_contact_host: string;
    sip_to_tag: string;
    sip_from_tag: string;
    sip_cseq: string;
    sip_call_id: string;
    switch_r_sdp: string;
    endpoint_disposition: string;
    last_sent_callee_id_name: string;
    last_sent_callee_id_number: string;
    sip_hangup_phrase: string;
    last_bridge_hangup_cause: string;
    last_bridge_proto_specific_hangup_cause: string;
    hangup_cause: string;
    hangup_cause_q850: string;
    digits_dialed: string;
    start_stamp: string;
    profile_start_stamp: string;
    answer_stamp: string;
    bridge_stamp: string;
    progress_media_stamp: string;
    end_stamp: string;
    start_epoch: string;
    start_uepoch: string;
    profile_start_epoch: string;
    profile_start_uepoch: string;
    answer_epoch: string;
    answer_uepoch: string;
    bridge_epoch: string;
    bridge_uepoch: string;
    last_hold_epoch: string;
    last_hold_uepoch: string;
    hold_accum_seconds: string;
    hold_accum_usec: string;
    hold_accum_ms: string;
    resurrect_epoch: string;
    resurrect_uepoch: string;
    progress_epoch: string;
    progress_uepoch: string;
    progress_media_epoch: string;
    progress_media_uepoch: string;
    end_epoch: string;
    end_uepoch: string;
    caller_id: string;
    duration: string;
    billsec: string;
    progresssec: string;
    answersec: string;
    waitsec: string;
    progress_mediasec: string;
    flow_billsec: string;
    mduration: string;
    billmsec: string;
    progressmsec: string;
    answermsec: string;
    waitmsec: string;
    progress_mediamsec: string;
    flow_billmsec: string;
    uduration: string;
    billusec: string;
    progressusec: string;
    answerusec: string;
    waitusec: string;
    progress_mediausec: string;
    flow_billusec: string;
    bridge_hangup_cause: string;
    call_uuid: string;
    sip_hangup_disposition: string;
    rtp_audio_in_raw_bytes: string;
    rtp_audio_in_media_bytes: string;
    rtp_audio_in_packet_count: string;
    rtp_audio_in_media_packet_count: string;
    rtp_audio_in_skip_packet_count: string;
    rtp_audio_in_jitter_packet_count: string;
    rtp_audio_in_dtmf_packet_count: string;
    rtp_audio_in_cng_packet_count: string;
    rtp_audio_in_flush_packet_count: string;
    rtp_audio_in_largest_jb_size: string;
    rtp_audio_in_jitter_min_variance: string;
    rtp_audio_in_jitter_max_variance: string;
    rtp_audio_in_jitter_loss_rate: string;
    rtp_audio_in_jitter_burst_rate: string;
    rtp_audio_in_mean_interval: string;
    rtp_audio_in_flaw_total: string;
    rtp_audio_in_quality_percentage: string;
    rtp_audio_in_mos: string;
    rtp_audio_out_raw_bytes: string;
    rtp_audio_out_media_bytes: string;
    rtp_audio_out_packet_count: string;
    rtp_audio_out_media_packet_count: string;
    rtp_audio_out_skip_packet_count: string;
    rtp_audio_out_dtmf_packet_count: string;
    rtp_audio_out_cng_packet_count: string;
    rtp_audio_rtcp_packet_count: string;
    rtp_audio_rtcp_octet_count: string;
}

export interface OriginatorCallerProfile {
    username: string;
    dialplan: string;
    caller_id_name: string;
    ani: string;
    aniii: string;
    caller_id_number: string;
    network_addr: string;
    rdnis: string;
    destination_number: string;
    uuid: string;
    source: string;
    context: string;
    chan_name: string;
}

export interface Originator {
    originator_caller_profiles: Array<OriginatorCallerProfile>;
}

export interface CallerProfile {
    username: string;
    dialplan: string;
    caller_id_name: string;
    ani: string;
    aniii: string;
    caller_id_number: string;
    network_addr: string;
    rdnis: string;
    destination_number: string;
    uuid: string;
    source: string;
    context: string;
    chan_name: string;
    originator: Originator;
}

export interface Times {
    created_time: string;
    profile_created_time: string;
    progress_time: string;
    progress_media_time: string;
    answered_time: string;
    bridged_time: string;
    last_hold_time: string;
    hold_accum_time: string;
    hangup_time: string;
    resurrect_time: string;
    transfer_time: string;
}

export interface Callflow {
    dialplan: string;
    profile_index: string;
    caller_profile: CallerProfile;
    times: Times;
}

export interface CDR {
    'core-uuid': string;
    switchname: string;
    channel_data: ChannelData;
    callStats: CallStats;
    variables: Variables;
    callflow: Array<Callflow>;
}